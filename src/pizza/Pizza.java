package pizza;

import javafx.beans.property.SimpleStringProperty;

public class Pizza {
	 
    private SimpleStringProperty pizza;
    private SimpleStringProperty prix;
    private SimpleStringProperty quantite;

    public Pizza(String pizza, String quantite , String prix) {
        this.pizza = new SimpleStringProperty(pizza);
        this.prix = new SimpleStringProperty(prix);
        this.quantite = new SimpleStringProperty(quantite);
    }

	public void setPizza(String pizza) {
		this.pizza = new SimpleStringProperty(pizza);
	}

	public void setPrix(String prix) {
		this.prix = new SimpleStringProperty(prix);
	}

	public void setQuantite(String quantite) {
		this.quantite = new SimpleStringProperty(quantite);
	}
	
	public String getPizza() {
		return pizza.get();
	}

	public String getPrix() {
		return prix.get();
	}

	public String getQuantite() {
		return quantite.get();
	}

  
}

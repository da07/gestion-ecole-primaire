package pizza;
import projet.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.*;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Controller  implements Initializable  {

	@FXML
	TextField t ;
	@FXML
	PasswordField p;
	@FXML
	TextField username ;
	@FXML
	TextField email ;

	
	public void connect(ActionEvent event) throws IOException {
		projet.Historique h = new projet.Historique();
		BufferedReader in = new BufferedReader(new FileReader("LoginClt.txt"));
		String line = "";
		if (t.getText().trim().isEmpty() || p.getText().trim().isEmpty()){
			 Alert fail= new Alert(AlertType.INFORMATION);
		     fail.setHeaderText("SIGN IN ERROR");
		     fail.setContentText("Username or Password can't be empty");
		     fail.showAndWait();	
		}
		else {
			line = in.readLine();
			while(!line.equals(t.getText())){
			line = in.readLine();
			if (line == null){
		        Alert fail= new Alert(AlertType.INFORMATION);
		        fail.setHeaderText("ALERT");
		        fail.setContentText("Username or Password is incorrect");
		        fail.showAndWait();
		        break ;
			}
			}
			if(line != null){
			line = in.readLine();
			if(line.equals("admin1")) {
					Parent root = FXMLLoader.load(getClass().getResource("EspaceAdmin.fxml"));
					Scene EspaceClientScene = new Scene(root);
					Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
					window.setScene(EspaceClientScene);
					window.show();	
			}
			else {
			if(line.equals(p.getText())){
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("EspaceClient.fxml"));  
				Parent root = (Parent)fxmlLoader.load(); 
				ClientController Cclt = fxmlLoader.<ClientController>getController() ;
				Cclt.setUsername(t.getText());
				Scene EspaceClientScene = new Scene(root);
				Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
				window.setScene(EspaceClientScene);
				window.show();	
			}
			else {
		        Alert fail= new Alert(AlertType.INFORMATION);
		        fail.setHeaderText("SIGN IN ERROR");
		        fail.setContentText("Username or Password is incorrect");
		        fail.showAndWait();	
			}
			}
			}
			}
			}

	public void SignUp(ActionEvent event) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SignUp.fxml")); 
		Parent root = (Parent)fxmlLoader.load(); 
		SignupController SignupC = fxmlLoader.<SignupController>getController() ;
		SignupC.setUsername(username.getText());
		SignupC.setEmail(email.getText());
		Scene SignUp = new Scene(root);
		SignUp.getStylesheets().add(getClass().getResource("TabPaneStyle.css").toExternalForm());
		Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
		window.setScene(SignUp);
		window.show();
	}
	

	

@Override
public void initialize(URL arg0, ResourceBundle arg1){
	// TODO Auto-generated method stub
}

	
}
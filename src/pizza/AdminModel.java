package pizza;

import javafx.beans.property.SimpleStringProperty;

public class AdminModel {
    private SimpleStringProperty fname;
    private SimpleStringProperty lname;
    private SimpleStringProperty username;
    private SimpleStringProperty passwd;

    public AdminModel(String fname, String lname , String username , String passwd) {
        this.fname = new SimpleStringProperty(fname);
        this.lname = new SimpleStringProperty(lname);
        this.username = new SimpleStringProperty(username);
        this.passwd = new SimpleStringProperty(passwd);
        
    }

	public void setFname(String fname) {
		this.fname = new SimpleStringProperty(fname);
	}

	public void setLname(String lname) {
		this.lname = new SimpleStringProperty(lname);
	}

	public void setUsername(String username) {
		this.username = new SimpleStringProperty(username);
	}
	
	public void setPasswd(String passwd) {
		this.passwd = new SimpleStringProperty(passwd);
	}
	
	public String getFname() {
		return fname.get();
	}

	public String getLname() {
		return lname.get();
	}

	public String getUsername() {
		return username.get();
	}
	
	public String getPasswd() {
		return passwd.get();
	}

}

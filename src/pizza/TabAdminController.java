package pizza;


import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import projet.Administrateur;
import projet.CommandeFinal;
import projet.Historique;
import projet.LigneCommande;

public class TabAdminController implements Initializable {
	@FXML TextField fname;
	@FXML TextField lname ;
	@FXML TextField username;
	@FXML TextField passwd ;
	@FXML TableView<AdminModel> Tab; 
	@FXML TableColumn<AdminModel , String> FnameColumn ;
	@FXML TableColumn<AdminModel , String> LnameColumn ;
	@FXML TableColumn<AdminModel , String> UsernameColumn ;
	@FXML TableColumn<AdminModel , String> PasswdColumn ;
	ObservableList<AdminModel> data= FXCollections.observableArrayList();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		FnameColumn.setCellValueFactory(new PropertyValueFactory<AdminModel , String>("fname") );
		LnameColumn.setCellValueFactory(new PropertyValueFactory<AdminModel , String>("lname") );
		UsernameColumn.setCellValueFactory(new PropertyValueFactory<AdminModel , String>("username") );
		PasswdColumn.setCellValueFactory(new PropertyValueFactory<AdminModel , String>("passwd") );
		
		FnameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		LnameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		UsernameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		PasswdColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		
		
		Object obj = null;
		Administrateur c = new Administrateur();
		try {
		FileInputStream fis = new FileInputStream( new File("Admin.txt"));       
	      while(true){
	      ObjectInputStream ois = new ObjectInputStream(fis);
	      obj = ois.readObject();
	      c = (Administrateur) obj;
	      data.add(new AdminModel(c.getnom(), c.getprenom(), c.getpseudo(), c.getPasswd()));
	      }		
	      }
		
	      catch (EOFException e){
	    	 
	      }
		catch (IOException e){
			e.printStackTrace();
		}
		catch (ClassNotFoundException e){
			e.printStackTrace();
		}
		
		
		Tab.setItems(data);
	}
	
	
	public void addAdmin() {
		Tab.getItems().add(new AdminModel(fname.getText() , lname.getText() , username.getText() , passwd.getText()));
	}
	
	public void updateFname(CellEditEvent cedit) {
		AdminModel p = Tab.getSelectionModel().getSelectedItem();
		p.setFname(cedit.getNewValue().toString());
	}
	public void updateLname(CellEditEvent cedit) {
		AdminModel p = Tab.getSelectionModel().getSelectedItem();
		p.setLname(cedit.getNewValue().toString());
	}
	public void updateUsername(CellEditEvent cedit) {
		AdminModel p = Tab.getSelectionModel().getSelectedItem();
		p.setUsername(cedit.getNewValue().toString());
	}
	
	public void updatePasswd(CellEditEvent cedit) {
		AdminModel p = Tab.getSelectionModel().getSelectedItem();
		p.setPasswd(cedit.getNewValue().toString());
	}
	
	public void deleteAdmin () {
		AdminModel selectedItem = Tab.getSelectionModel().getSelectedItem();
		Tab.getItems().remove(selectedItem);
	}
	public void saveAdmins() {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileOutputStream(
				    new File("LoginAdmin.txt"), 
				    true ));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		Historique h = new Historique();
		for (int i = 0 ; i < Tab.getItems().size(); i++) {
			AdminModel A = Tab.getItems().get(i);
			Administrateur a = new Administrateur(A.getFname() , A.getLname() , A.getUsername() , A.getPasswd());
			h.ecrireAdmin(a);
			out.println(A.getFname());
			out.println(A.getPasswd());
			out.close();
		}
	}

}

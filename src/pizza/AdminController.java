package pizza;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.control.ListView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;


public class AdminController implements Initializable  {
	@FXML
	ListView<String> lv;
	@FXML AnchorPane tabContainer  ;
	Pane tab = null;
	ObservableList<String> view = FXCollections.observableArrayList();
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
       	view.addAll("Admins" , "Employees" , "Clients" , "Pizza Orders");
       	lv.setItems(view);
		lv.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
		    @Override
		    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		        // Your action here
		    	if (newValue.equals("Admins")) {
				
				try {
					tab = FXMLLoader.load(getClass().getResource("tab.fxml"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				tabContainer.getChildren().clear();
		       	tabContainer.getChildren().add(tab);
		    	}
		    	else if (newValue.equals("Clients")) {
					try {
						tab = FXMLLoader.load(getClass().getResource("clients.fxml"));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					tabContainer.getChildren().clear();
			       	tabContainer.getChildren().add(tab);	    	}
		    	else if (newValue.equals("Pizza Orders")) {
					try {
						tab = FXMLLoader.load(getClass().getResource("orders.fxml"));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					tabContainer.getChildren().clear();
			       	tabContainer.getChildren().add(tab);	    	}
		    	else {
					try {
						tab = FXMLLoader.load(getClass().getResource("employee.fxml"));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					tabContainer.getChildren().clear();
			       	tabContainer.getChildren().add(tab);	    	}
		    }
		});

       	}


}

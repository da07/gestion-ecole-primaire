package pizza;
import projet.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.*;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


public class ClientController implements Initializable{
	@FXML TextField id ; 
	@FXML TextField username ;
	@FXML TextField mail ;
	@FXML TextField adress;

	@FXML TableView<pizza.ModelCommande> Tab; 
	@FXML TableColumn<pizza.ModelCommande , String> PizzaColumn ;
	@FXML TableColumn<pizza.ModelCommande , String> PrixColumn ;
	@FXML TableColumn<pizza.ModelCommande , String> QteColumn ;
	@FXML TableColumn<pizza.ModelCommande , String> TailleColumn ;
	ObservableList<pizza.ModelCommande> data= FXCollections.observableArrayList();
	
	public void setUsername(String username) {
		this.username.setText(username);
	}
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		PizzaColumn.setCellValueFactory(new PropertyValueFactory<ModelCommande , String>("pizza") );
		PrixColumn.setCellValueFactory(new PropertyValueFactory<ModelCommande , String>("prix") );
		QteColumn.setCellValueFactory(new PropertyValueFactory<ModelCommande , String>("quantite") );
		TailleColumn.setCellValueFactory(new PropertyValueFactory<ModelCommande , String>("taille") );
  	  Platform.runLater(new Runnable() {
		  @Override public void run() {
			  	Object obj = null;
				Client c = new Client();
				try {
				FileInputStream fis = new FileInputStream( new File("Client.txt"));       
			      while(true){
			      ObjectInputStream ois = new ObjectInputStream(fis);
			      obj = ois.readObject();
			      c = (Client) obj;
			      if(new String(username.getText()).equals(c.getpseudo())){
			    	  id.setText(c.getIdClt());
			    	  adress.setText(c.getAdress());
			    	  mail.setText(c.getEmail());
				      break;
			      }
			      
			      }
			      }		
				
			      catch (EOFException e){
			    	 
			      }
				catch (IOException e){
					e.printStackTrace();
				}
				catch (ClassNotFoundException e){
					e.printStackTrace();
				} 
		  }
		  
	  });
		Object obj = null;


		LigneCommande cm = new LigneCommande();
		try {
		FileInputStream fis = new FileInputStream( new File("Commande.txt"));       
	      while(true){
	      ObjectInputStream ois = new ObjectInputStream(fis);
	      obj = ois.readObject();
	      cm = (LigneCommande) obj;
	      data.add(new ModelCommande(cm.getP().getnom(), cm.getP().gettaille() , Float.toString(cm.getP().getprix()), Integer.toString(cm.getQte())));
	      }		
	      }
		
	      catch (EOFException e){
	    	 
	      }
		catch (IOException e){
			e.printStackTrace();
		}
		catch (ClassNotFoundException e){
			e.printStackTrace();
		}
		
		
		Tab.setItems(data);
	}
	public void deconnect(ActionEvent event) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
		Scene MainView = new Scene(root);
		Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
		window.setScene(MainView);
		window.show();
	}
	
	public void ShowMenu(ActionEvent event) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("MenuPizza.fxml"));
		Stage window = new Stage(); 
		Scene MenuView = new Scene(root);

		window.initModality(Modality.APPLICATION_MODAL);

		window.setScene(MenuView);
		window.showAndWait();
	}
	
	public void commande(ActionEvent event) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("commande.fxml"));
		Stage window = new Stage(); 
		Scene CommandeView = new Scene(root);

		window.initModality(Modality.APPLICATION_MODAL);
		window.setScene(CommandeView);
		window.showAndWait();
	}
	
	public void lireOrder() {
		
	}
}

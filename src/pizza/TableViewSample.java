package pizza;
import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URL;
import java.util.ResourceBundle;

import pizza.Pizza;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class TableViewSample implements Initializable {
	
	@FXML TableView<Pizza> Tab; 
	@FXML TableColumn<Pizza , String> PizzaColumn ;
	@FXML TableColumn<Pizza , String> PrixColumn ;
	@FXML TableColumn<Pizza , String> QteColumn ;
	ObservableList<Pizza> data= FXCollections.observableArrayList();
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		PizzaColumn.setCellValueFactory(new PropertyValueFactory<Pizza , String>("pizza") );
		PrixColumn.setCellValueFactory(new PropertyValueFactory<Pizza , String>("prix") );
		QteColumn.setCellValueFactory(new PropertyValueFactory<Pizza , String>("quantite") );

		Object obj = null;
		projet.Pizza c = new projet.Pizza();
		try {
		FileInputStream fis = new FileInputStream( new File("Histo.txt"));       
	      while(true){
	      ObjectInputStream ois = new ObjectInputStream(fis);
	      obj = ois.readObject();
	      c = (projet.Pizza) obj;
	      data.add(new Pizza(c.getnom() , c.gettaille() , Float.toString(c.getprix())));
	      }		
	      }
		
	      catch (EOFException e){
	    	 
	      }
		catch (IOException e){
			e.printStackTrace();
		}
		catch (ClassNotFoundException e){
			e.printStackTrace();
		}
		
		
		Tab.setItems(data);
	}

}

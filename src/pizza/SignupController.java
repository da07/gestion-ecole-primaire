package pizza;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import projet.Client;
import projet.Historique;

public class SignupController implements Initializable{
	@FXML
	TabPane Tp;
	@FXML
	Tab t1;
	@FXML
	Tab t2;
	@FXML TextField username ;
	@FXML TextField fname;
	@FXML TextField lname;
	@FXML TextField adress ;
	@FXML TextField email;
	@FXML PasswordField pass ;
	@FXML PasswordField passconf;
	
	public void setUsername(String username) {
		this.username.setText(username);
	}
	
	public void setEmail(String email) {
		this.email.setText(email);
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		username.setDisable(true);
		email.setDisable(true);
		// TODO Auto-generated method stub
		
	}
	
	public void SwitchTabs() throws IOException {
		if(username.getText().trim().isEmpty() || fname.getText().trim().isEmpty() || lname.getText().trim().isEmpty() ||adress.getText().trim().isEmpty() || email.getText().trim().isEmpty()){
		        Alert fail= new Alert(AlertType.INFORMATION);
		        fail.setHeaderText("ALERT");
		        fail.setContentText("Please fill all the fields");
		        fail.showAndWait();
		}
		else {
		t1.setDisable(true);
		t2.setDisable(false);
		Tp.getSelectionModel().select(t2);
		}
	}
	
	public void Back() throws IOException {
		t2.setDisable(true);
		Tp.getSelectionModel().select(t1);
		t1.setDisable(false);
	}
	
	public void finish(ActionEvent event) throws IOException {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileOutputStream(
				    new File("LoginClt.txt"), 
				    true ));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		Client a = new Client();
		Client c = new Client(lname.getText(), fname.getText() , username.getText() ,a.generateid(10) , adress.getText() , email.getText() );
		Historique h = new Historique();
		h.EcrireClient(c);
		out.println(username.getText());
		out.println(pass.getText());
		out.close();
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("EspaceClient.fxml"));   
		Parent root = (Parent)fxmlLoader.load(); 
		ClientController Cclt = fxmlLoader.<ClientController>getController() ;
		Cclt.setUsername(username.getText());
		Scene EspaceClientScene = new Scene(root);
		Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
		window.setScene(EspaceClientScene);
		window.show();
	}
}

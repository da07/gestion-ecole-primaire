package pizza;
import projet.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ResourceBundle;


import javafx.scene.*;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
public class CommandeController implements Initializable{
	int a ;
	@FXML TextField id ;
	@FXML Label stext;
	@FXML Slider s;
	@FXML TableView<pizza.ModelCommande> Tab; 
	@FXML TableColumn<pizza.ModelCommande , String> PizzaColumn ;
	@FXML TableColumn<pizza.ModelCommande , String> PrixColumn ;
	@FXML TableColumn<pizza.ModelCommande , String> QteColumn ;
	@FXML TableColumn<pizza.ModelCommande , String> TailleColumn ;
	ObservableList<pizza.ModelCommande> data= FXCollections.observableArrayList();
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		CommandeFinal cf = new CommandeFinal();
		id.setText(cf.generate(10));
		id.setEditable(false);
		// TODO Auto-generated method stub
		PizzaColumn.setCellValueFactory(new PropertyValueFactory<pizza.ModelCommande , String>("pizza") );
		PrixColumn.setCellValueFactory(new PropertyValueFactory<pizza.ModelCommande , String>("prix") );
		QteColumn.setCellValueFactory(new PropertyValueFactory<pizza.ModelCommande , String>("quantite") );
		TailleColumn.setCellValueFactory(new PropertyValueFactory<pizza.ModelCommande , String>("taille") );
		
		PizzaColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		PrixColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		QteColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		TailleColumn.setCellFactory(TextFieldTableCell.forTableColumn());
		
	}
	public void updateText() {
        s.valueProperty().addListener(new ChangeListener<Object>() {

			@Override
			public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
				// TODO Auto-generated method stub
                stext.textProperty().setValue(
                        String.valueOf((int) s.getValue()));
                
                float b =  Float.parseFloat(String.valueOf(s.getValue()));
                a= (int) b;
        		while (data.size() < a) {
        			data.add(new ModelCommande("NomP", "Taille", "Prix" , "Quantite"));
        		}
        		Tab.setItems(data);
			}
        });

	}
	
	public void updatePizza(CellEditEvent cedit) {
		pizza.ModelCommande p = Tab.getSelectionModel().getSelectedItem();
		p.setPizza(cedit.getNewValue().toString());
	}
	public void updatePrix(CellEditEvent cedit) {
		pizza.ModelCommande p = Tab.getSelectionModel().getSelectedItem();
		p.setPrix(cedit.getNewValue().toString());
	}
	public void updateTaille(CellEditEvent cedit) {
		pizza.ModelCommande p = Tab.getSelectionModel().getSelectedItem();
		p.setTaille(cedit.getNewValue().toString());
	}
	
	public void updateQte(CellEditEvent cedit) {
		pizza.ModelCommande p = Tab.getSelectionModel().getSelectedItem();
		p.setQuantite(cedit.getNewValue().toString());
	}
	
	public void PassOrder() {
		CommandeFinal cf = new CommandeFinal();
		Historique h = new Historique();
		for (int i = 0 ; i < Tab.getItems().size(); i++) {
			pizza.ModelCommande P = Tab.getItems().get(i);
			LigneCommande L = new LigneCommande("1", P.getPizza(), P.getTaille() , P.getPrix() ,Integer.parseInt(P.getQuantite()));
			cf.ajouterLigneCommande(L);
			h.EcrireCommande(cf);
		}
		
	}
}

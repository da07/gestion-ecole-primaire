package pizza;

import javafx.beans.property.SimpleStringProperty;

public class ModelCommande {
    private SimpleStringProperty pizza;
    private SimpleStringProperty prix;
    private SimpleStringProperty taille;
    private SimpleStringProperty quantite;

    public ModelCommande(String pizza, String taille , String prix , String quantite) {
        this.pizza = new SimpleStringProperty(pizza);
        this.prix = new SimpleStringProperty(prix);
        this.taille = new SimpleStringProperty(taille);
        this.quantite = new SimpleStringProperty(quantite);
    }

	public void setPizza(String pizza) {
		this.pizza = new SimpleStringProperty(pizza);
	}

	public void setPrix(String prix) {
		this.prix = new SimpleStringProperty(prix);
	}

	public void setQuantite(String quantite) {
		this.quantite = new SimpleStringProperty(quantite);
	}
	
	public void setTaille(String taille) {
		this.taille = new SimpleStringProperty(taille);
	}
	
	public String getPizza() {
		return pizza.get();
	}

	public String getPrix() {
		return prix.get();
	}

	public String getQuantite() {
		return quantite.get();
	}
	
	public String getTaille() {
		return taille.get();
	}


}

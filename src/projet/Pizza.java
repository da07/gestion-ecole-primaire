package projet;
import java.io.*;
import java.util.Scanner ;

public class Pizza implements Serializable {
	private String id , nom , taille ; 
	private float prix ;
	public Pizza() {
		id = nom = taille = "";
		prix = (float) 0.0;
	}
	public Pizza(String a , String b , String c , float d){
		id =a ; 
		nom = b ;
		taille = c ;
		prix = d ;
	}
	public void saisie(){
		Scanner sc = new Scanner(System.in);
		System.out.println("Saisie des informations d'une pizza\n");
		System.out.println("Id : ");
		String a = sc.next();
		System.out.println("Nom : ");
		String b = sc.next();
		System.out.println("Taille : ");
		String c = sc.next();
		System.out.println("Prix : ");
		float d = sc.nextFloat();
		id = a; nom = b; taille = c; prix = d;
	}
	public void saisie_pour_client(){
		Scanner sc = new Scanner(System.in);
		System.out.println("Nom : ");
		String a = sc.next();
		System.out.println("Taille : ");
		String b = sc.next();
		nom = a; taille = b ; 
	}
	public String toString() {
		return 
		"Id : "+ id +
		"\nNom : "+nom +
		"\nTaille : "+taille +
		"\nPrix : "+prix +"\n";
	}
	//getteurs & setteurs
	public void setid(String a) {id =a ;}
	public String getid() {return id ;}
	public void setnom(String a) {nom = a;}
	public String getnom() {return nom ;}
	public void settaille(String a) {taille = a; }
	public String gettaille() {return taille ;}
	public void setprix(float a) {prix = a;} 
	public float getprix() {return prix ;}
	
	
	

}

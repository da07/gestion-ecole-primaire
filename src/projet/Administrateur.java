package projet;
import java.util.Scanner ;
import java.io.*;
public class Administrateur extends Info  {
	private String passwd ;
	public Administrateur(String a , String b , String c , String d) {
		super(a,b,c);
		passwd = d;
	}
	public Administrateur() {
		super();
		passwd = "";
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	

}

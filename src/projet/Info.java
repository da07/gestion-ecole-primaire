package projet;

import java.io.Serializable;
import java.util.Scanner;

public abstract class Info implements Serializable{
	protected String nom;
	protected String prenom;
	protected String pseudo ;
	Info(String a , String b , String c) {
		nom = a ; 
		prenom = b;
		pseudo = c;
	}
	Info() {
		nom = "" ; 
		prenom = "";
		pseudo = "";
	}
	//getteurs & setteurs 
	public void setnom(String a) {nom = a;}
	public String getnom(){return nom;}
	public void setprenom(String a) {prenom = a;}
	public String getprenom(){return prenom;}
	public void setpseudo(String a) {pseudo = a;}
	public String getpseudo(){return pseudo;}
	
	public void saisie() {
		Scanner X = new Scanner(System.in);
		System.out.println("Nom :");
		String a = X.next();
		System.out.println("Prenom :");
		String b = X.next();
		System.out.println("Pseudo :");
		String ps = X.next();
		nom = a;
		prenom = b ;
		pseudo = ps;
		
	}
	public String toString(){
		return ("Nom : " + this.nom +
		"\nPrenom : "+ this.prenom +
		"\nPseudo : "+ this.pseudo 
				);
	}
	

}


package projet;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator; 
import java.util.Set;
import java.util.Map.Entry;

public class ListeClients {
	Map <String ,Client> m = new HashMap<>();
	public void AjouterClt(){
		Client c = new Client();
		c.Saisie();
		m.put(c.getidclt(), c);
	}
	public boolean supprimerClt(Client c){
		boolean test;
		test = m.remove(c.getidclt(), c);
		return test;
	}
	public boolean RechercheClt(String id){
		boolean test;
		test = m.containsKey(id);
		return test;
	}
	public void AfficherClients() {
		int i = 1 ;
		System.out.println("Affichage des Clients \n");
		Set<Entry<String, Client>> setm = m.entrySet();
	    Iterator<Entry<String, Client>> it = setm.iterator();
	    while(it.hasNext()){
	    	Entry<String , Client> e = it.next();
	    	System.out.println("Client " +i);
	    	System.out.println(e.getValue());
	    	i++;
	    }
	}
	
}


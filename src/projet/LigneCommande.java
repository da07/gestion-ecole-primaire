package projet;
import java.util.Scanner;
import java.io.*;
public class LigneCommande implements Serializable  {
	Pizza p;
	int qte;
	static final long serialVersionUID =  571579866129894257L;
	public LigneCommande() {
		this(null, null , null , "0.0" , 0);
		
	};
	public LigneCommande(String aa , String b , String c , String d , int e){
		p = new projet.Pizza(aa , b , c , Float.parseFloat(d));
		qte = e;
	}
	public Pizza getP() {
		return p;
	}
	public void setP(Pizza p) {
		this.p = p;
	}
	public int getQte() {
		return qte;
	}
	public void setQte(int qte) {
		this.qte = qte;
	}
	public void saisie_ligne() {
		Scanner sc = new Scanner(System.in);
		p = new Pizza();
		p.saisie();
		System.out.println("Quantite: ");
		qte = sc.nextInt();
		}
	public String toString() {
		return p.toString() + "Quantite : "+ qte +"\n" ;
	}
}

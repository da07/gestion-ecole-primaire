package projet;
import java.io.*;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Iterator;

public class ListeOuvriers {
	Map <String , Ouvrier> m = new HashMap<>();
	public void ajouterOuvrier (){
		Ouvrier o = new Ouvrier();
		o.saisie_ouvrier();
		m.put(o.getid(), o);
	}
	public boolean supprimerOuvrier(Ouvrier o){
		boolean test;
		test = m.remove(o.getid(), o);
		return test;
	}
	public void AfficherOuvrier() {
		int i = 1 ;
		System.out.println("Affichage des Ouvrier \n");
		Set<Entry<String, Ouvrier>> setm = m.entrySet();
	    Iterator<Entry<String, Ouvrier>> it = setm.iterator();
	    while(it.hasNext()){
	    	Entry<String , Ouvrier> e = it.next();
	    	System.out.println("Ouvrier " +i);
	    	System.out.println(e.getValue());
	    	i++;
	    }
	}
}

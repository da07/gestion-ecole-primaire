package projet;
import java.util.HashMap;
import java.io.*;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;


public class Historique {
	public void EcrireCommande(CommandeFinal cf) {
	    ObjectOutputStream oos;
	    Set<Entry<String, LigneCommande>> setm = cf.m.entrySet();
	    Iterator<Entry<String, LigneCommande>> it = setm.iterator();
	    try {
	      oos = new ObjectOutputStream(
	              new BufferedOutputStream(
	                new FileOutputStream(
	                  new File("Commande.txt"),true)));
	      while(it.hasNext()){
	    	  Entry<String , LigneCommande> e = it.next();
	    	  oos.writeObject(e.getValue());
	      }
	      oos.close();
	    }
	    catch(IOException e){
	    	e.printStackTrace();
	    }
	    
	}
	public void EcrireClient(Client c) {
	    ObjectOutputStream oos;
	    try {
	      oos = new ObjectOutputStream(
	              new BufferedOutputStream(
	                new FileOutputStream(
	                  new File("Client.txt"),true)));
	    	  oos.writeObject(c);
	      oos.close();
	    }
	    catch(IOException e){
	    	e.printStackTrace();
	    }
	    
	}
	public  void Lirecommande() {
		ObjectInputStream ois;
		Object obj = null;
		int i = 1;
		try {
	      ois = new ObjectInputStream(
	              new BufferedInputStream(
	                new FileInputStream(
	                  new File("Histo.txt"))));
	      System.out.println("Affichage des commandes :\n") ;
	      while((obj = ois.readObject())!= null){

	    	  System.out.println("LigneCommande " + i );
	    	  System.out.println(obj.toString());
	    	  i++;
	    	  }
	    
	      

		}
	      catch (EOFException e){
	    	 
	      }
		catch (IOException e){
			e.printStackTrace();
		}
		catch (ClassNotFoundException e){
			e.printStackTrace();
		}
	}
	public void EcrirePizza(projet.Pizza p) {
	    ObjectOutputStream oos;
	    try {
	      oos = new ObjectOutputStream(
	              new BufferedOutputStream(
	                new FileOutputStream(
	                  new File("Histo.txt"), true)));
	      oos.writeObject(p);
	      oos.close();
	    }
	    catch(IOException e){
	    	e.printStackTrace();
	    }
	}
	public  void LirePizza() {
		ObjectInputStream ois;
		Object obj = null;
		try {
	      ois = new ObjectInputStream(
	              new BufferedInputStream(
	                new FileInputStream(
	                  new File("Histo.txt"))));
	      while((obj = ois.readObject())!= null){
	    	  System.out.println(obj.toString());
	    	  }
	    
	      

		}
	      catch (EOFException e){
	    	 
	      }
		catch (IOException e){
			e.printStackTrace();
		}
		catch (ClassNotFoundException e){
			e.printStackTrace();
		}
	}
	
	public void ecrireAdmin(Administrateur a) {
	    ObjectOutputStream oos;
	    try {
	      oos = new ObjectOutputStream(
	              new BufferedOutputStream(
	                new FileOutputStream(
	                  new File("Admin.txt"))));
	      oos.writeObject(a);
	      oos.close();
	    }
	    catch(IOException e){
	    	e.printStackTrace();
	    }
	}
}
